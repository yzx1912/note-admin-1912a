import { defineStore } from 'pinia';
import { user } from '@/services';
import { getStatus, getUserList, AddPass, RefusePass, searchComment, delComment, AddComment } from '@/services/user';

const userStore = defineStore<string, any>('baseStore', {
  state: () => ({
    pageList: [], // 页面管理
    NewArticleList: [], //工作台 最新文章
    NewCommentList: [], //工作台 最新评论
    commentList: [], //评论管理
    searchList: [], // 搜索记录
    userList: [], // 用户管理
    viewList: [], // 访问统计
    skList: [], // 知识小册列表数据
    total: 0, // 数据总条数
    TagLabelList: [], //标签管理页面
    CategoryLabelList: [], //分类管理页面
    filePage: [], //文件管理页面
    articleAll: [], // 所有文章
    SmtpViews: [], //邮件管理
    PosterViews: [], //海报管理
    editList: [],
    NewArticlelenght: '', //文章总数
    commentLenght: '', //评论总数
    tagLenght: '', //标签总数
    fileLenght: '', //文件总数
    geteLenght: '', //分类总数
  }),
  actions: {
    // 页面管理
    async getPageList(params: any) {
      const { data } = await user.getPage(params);
      this.$patch({
        pageList: data[0],
        total: data[1],
      });
      return data[0];
    },
    //页面管理删除
    async DelPage(id: string) {
      const { data } = await user.delPage(id)
    },
    // 页面管理发布
    async pageStatus(id: string, params: any) {
      const { data } = await user.pageStatus(id, params)
    },

    async getNewArticle() {
      //工作台 最新文章
      const { data } = await user.NewArticle({});
      this.$patch({
        NewArticleList: data[0],
        NewArticlelenght: data[1],
      });
      return data[0];
    },
    async getNewComment() {
      //工作台 最新评论
      const { data } = await user.NewComment({});
      this.$patch({
        NewCommentList: data[0],
      });
      return data[0];
    },
    //工作台最新评论的删除
    async getNewCommentDetil(id: string) {
      const { data } = await user.NewCommentDel(id);
    },

    //全部文章页面
    async getArticleAll(payload: any) {
      const { data } = await user.getArticleAll(payload);
      this.$patch({
        articleAll: data[0],
        total: data[1],
      });
    },
    // 所有文章首焦推荐
    async articleRecommend(id: string, params: any) {
      const { data } = await user.articleRecommend(id, params)
    },
    // 所有文章删除
    async articleDel(id: string) {
      const { data } = await user.articleDel(id)
    },
    // 发布线上/设为草稿
    async getArticleStatus(id: string, params: any) {
      const { data } = await user.getArticleStatus(id, params)
    },

    // 搜索记录
    async getSearch(params: any) {
      const { data } = await user.getSearch(params);
      this.$patch({
        searchList: data[0],
        total: data[1],
      });
    },
    // 搜索记录删除
    async GetSearchDetil(id: string) {
      const { data } = await user.getSearchDel(id);
    },
    // 用户管理
    async getUserList(params: any) {
      const { data } = await user.getUserList(params);
      this.$patch({
        userList: data[0],
        total: data[1],
      });
    },
    //用户管理的禁用启用
    async getUserLisDisabled(params: any) {
      console.log(params);

      const { data } = await user.getUserDisabled(params);
    },

    // 访问统计
    async getView(params: any) {
      const { data } = await user.getView(params);
      this.$patch({
        viewList: data[0],
        total: data[1],
      });
    },
    // 知识小册
    async getskListAction(page: number, pageSize: number, status?: string, title?: string) {
      const { data } = await user.getskList({ page, pageSize, status, title });
      this.$patch({
        skList: data[0],
        total: data[1],
      });
    },
    // 发布线上/设为草稿
    async getStatus(id: string, status: any) {
      const { data } = await user.getStatus(id, {
        status: status,
      });
    },
    // 知识小册删除
    async delKnowledge(id: string) {
      const { data } = await user.delKnowledge(id);
    },
    async getTagLabel() {
      //标签管理
      const { data } = await user.TagLabel({});
      this.$patch({
        TagLabelList: data,
        tagLenght: data.length,
      });
      return data;
    },
    //标签管理保存
    async addTagLabel(label: string, value: string) {
      const { data } = await user.addTagLabel({ label, value });
    },
    //标签管理删除
    async delTagLabel(id: string) {
      const { data } = await user.delTagLabel(id);
    },
    //分类管理
    async getCategoryLabel() {
      const { data } = await user.CategoryLabel({});
      this.$patch({
        CategoryLabelList: data,
        geteLenght: data.length,
      });
    },
    //分类管理保存
    async addLabel(label: string, value: string) {
      const { data } = await user.addLabel({ label, value });
    },
    //分类管理删除
    async delCategoryLabel(id: string) {
      const { data } = await user.delCategoryLabel(id);
    },
    //访问统计的删除
    async GetViewDetil(id: string) {
      const { data } = await user.getViewDel(id);
    },
    //文件管理
    async GetFilePage(page: number, pageSize: number, type?: string, originalname?: string) {
      const { data } = await user.getFile({ page, pageSize, type, originalname });
      this.$patch({
        filePage: data[0],
        total: data[1],
        fileLenght: data[1],
      });
      return data[0];
    },
    //文件管理的删除
    async GetFileDetil(id: string) {
      const { data } = await user.getFileDel(id);
    },

    //评论
    async getComment(payload: any) {
      const { data } = await user.getComment(payload);
      this.$patch({
        commentList: data[0],
        total: data[1],
        commentLenght: data[1],
      });
    },
    //评论的通过
    async AddPass(id: string, pass: any) {
      const { data } = await user.AddPass(id, {
        pass: true
      });
    },
    //评论的拒绝
    async RefusePass(id: string, pass: any) {
      const { data } = await user.RefusePass(id, {
        pass: false,
      });
    },
    //评论搜索
    async SearchComment(params: any) {
      const { data } = await user.searchComment(params);
      this.$patch({
        commentList: data[0],
      });
    },
    //评论删除
    async DelComment(id: string) {
      const { data } = await user.delComment(id);
    },
    //评论全部通过

  },
})

export default userStore;
