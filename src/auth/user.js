const authData = [
  //管理员
  {
    role: 'admin',
    viewList: [
      //文章管理
      {
        path: '/article',
        title: '文章管理',
        children: [
          {
            path: '/article/all',
            title: '所有文章',
          },
          {
            path: '/article/category',
            title: '分类管理',
          },
          {
            path: '/article/tags',
            title: '标签管理',
          },
        ],
      },
      //文章管理 -所有文章
      //页面管理
      {
        path: '/page',
        title: '页面管理',
      },
      {
        path: '/knowledge',
        title: '知识小册',
      },
      {
        path: '/poster',
        title: '海报管理',
      },
      {
        path: '/comment',
        title: '评论管理',
      },
      {
        path: '/mail',
        title: '邮件管理',
      },
      {
        path: '/file',
        title: '文件管理',
      },
      {
        path: '/search',
        title: '搜索记录',
      },
      {
        path: '/view',
        title: '访问统计',
      },
      {
        path: '/user',
        title: '用户管理',
      },
      {
        path: '/setting',
        title: '系统设置',
      },
    ],
    //接口请求
    apiList: [
      {
        // 所有文章页面api 编辑
        path: '/article/edit',
        url: 'api/article/:id',
        method: 'PATCH',
      },
      {
        // 所有文章页面api 删除
        path: '/article',
        url: '/api/article/:id',
        method: 'DELETE',
      },

      {
        // 分类管理页面api 添加标签
        path: '/category',
        url: '/api/category',
        method: 'POST',
      },
      {
        // 标签管理页面api 添加标签
        path: '/article/tags',
        url: '/api/tag',
        method: 'GET',
      },
      {
        // 页面管理页面api 编辑
        path: '/page/editor/:id',
        url: '/api/page/:id',
        method: 'PATCH',
      },
      {
        // 页面管理页面api 删除
        path: '/page',
        url: '/api/page/id',
        method: 'DELETE',
      },
      {
        // 知识小册api 新建
        path: '/knowledge',
        url: '/api/knowledge/book',
        method: 'POST',
      },
      {
        // 评论管理api 通过
        path: '/comment',
        url: '/api/comment/:id',
        method: 'PATCH',
      },
      {
        // 评论管理api 拒绝
        path: '/comment',
        url: '/api/comment/:id',
        method: 'PATCH',
      },
      {
        // 评论管理api 回复
        path: '/comment',
        url: '/api/comment',
        method: 'POST',
      },
      {
        // 评论管理页面api 删除
        path: '/comment',
        url: '/api/comment/:id',
        method: 'DELETE',
      },
      {
        // 搜索记录页面api 删除
        path: '/search',
        url: '/api/search/:id',
        method: 'DELETE',
      },
      {
        // 搜索记录页面api 搜索
        path: '/search',
        url: 'api/search?e=1&pagepagSize=12&keyword=111',
        method: 'GET',
      },
      {
        // 访问统计页面api 删除
        path: '/view',
        url: '/api/view/:id',
        method: 'DELETE',
      },
      {
        // 访问统计页面api 搜索
        path: '/view',
        url: '/api/view/:id',
        method: 'DELETE',
      },
      {
        // 用户管理页面api 禁用
        path: '/view',
        url: '/api/view?page=1&pageSize=12&os=Mac',
        method: 'GET',
      },
      {
        // 用户管理页面api 禁用
        path: '/user',
        url: '/api/user/update',
        method: 'POST',
      },
      {
        // 用户管理页面api 授权/解除授权
        path: '/user',
        url: '/api/user/update',
        method: 'POST',
      },
    ],
  }, //访客
  {
    role: 'visitor',
    //视图
    viewList: [
      {
        path: '/article',
        title: '文章管理',
        children: [
          {
            path: 'article/all',
            title: '所有文章',
          },
          {
            path: '/article/category',
            title: '分类管理',
          },
          {
            path: '/article/tags',
            title: '标签管理',
          },
        ],
      },
      {
        path: '/page',
        title: '页面管理',
      },
      {
        path: '/knowledge',
        title: '知识小册',
      },
      {
        path: '/poster',
        title: '海报管理', // 无
      },
      {
        path: '/comment',
        title: '评论管理', //无
      },
      {
        path: '/mail',
        title: '邮件管理', // 无
      },

      {
        path: '/file',
        title: '文件管理', //无
      },
      {
        path: '/search',
        title: '搜索纪录', // 无
      },
      {
        path: '/view',
        title: '访问统计', // 无
      },
    ],
    //api请求
    apiList: [
      {
        // 所有文章页面api 编辑
        path: '/article/edit',
        url: 'api/article/:id',
        method: 'PATCH',
      },
      // 所有文章页面 撤销首焦/删除 按钮 ---访客无权限
      // 查看访问
      // 页面管理 页面api 编辑/下线/删除 --- 访客无权限
      // 查看访问
      {
        // 所有文章页面api 访客统计
        path: '/page',
        url: 'api/view/url',
        method: 'GET',
      },
      // 知识小册api 新建 ---访客无权限
      {
        // 搜索记录页面api 搜索
        path: '/search',
        url: 'api/search?page=1&pageSize=12&keyword=111',
        method: 'GET',
      },
      // 搜索记录页面api 删除 ---访客无权限
      // 访问统计页面api 删除 ---访客无权限
      {
        // 访问统计页面api 搜索
        path: '/view',
        url: '/api/view/:id',
        method: 'DELETE',
      },
    ],
  },
];

export default authData;
