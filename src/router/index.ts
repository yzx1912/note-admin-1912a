import { createRouter, createWebHistory, createWebHashHistory, routerKey } from 'vue-router';
import BasicLayout from '../layouts/BasicLayout.vue';
import authList from '@/auth/user.js';
import notFoundVue from '../views/notFound.vue';
import viewConfig from '@/views/view.config';
// only githubpages preview site used, if use template please remove this check
// and use `createWebHistory` is recommend

const hasGithubPages = import.meta.env.VITE_GHPAGES;
const baseRouter = [
  {
    path: '/worker',
    name: 'worker',
    meta: { title: '工作台', icon: 'icon-icon-test' },
    component: () => import('@/views/worker/WorkerPage.vue'),
  },
];
const router = createRouter({
  history: hasGithubPages ? createWebHashHistory() : createWebHistory(),
  routes: [
    {
      path: '/login',
      name: 'login',
      meta: { title: '登录', showMenuItem: false },
      component: () => import('@/views/login/index.vue'),
    },
    {
      path: '/register',
      name: 'register',
      meta: { title: '注册', showMenuItem: false },
      component: () => import('@/views/register/index.vue'),
    },
    {
      path: '/page/editor',
      name: 'editor',
      meta: { title: '新增编辑', showMenuItem: false },
      component: () => import('@/views/editor/index.vue'),
    },
    {
      path: '/knowledge/editor/:id',
      name: 'edit',
      meta: { title: '小册编辑', showMenuItem: false },
      component: () => import('@/views/edit/index.vue'),
    },
    {
      path: '/',
      name: 'index',
      meta: { title: 'Home', showMenuItem: false },
      component: BasicLayout,
      redirect: '/worker',
      children: [
        ...baseRouter,
        {
          path: '/404',
          name: '404',
          meta: { title: '页面找不到', icon: 'icon-icon-test', showMenuItem: false },
          component: () => import('@/views/notFound.vue'),
        },
        {
          path: '/ownspace',
          name: 'ownspace',
          meta: { title: '个人中心', showMenuItem: false },
          component: () => import('@/views/ownspace/index.vue'),
        },
      ],
    },
  ],
});
const formatName = (val: string) =>
  val
    .split('/')
    .filter(v => v)
    .map((item, i) => (i === 0 ? item : item[0].toUpperCase() + item.slice(1)))
    .join('');

const wihleList = ['/login', '/registry'];

const menuItemChildren = arr =>
  arr.map(({ path, title, children }: any) => {
    const name = formatName(path);
    const item = {
      path,
      name,
      meta: {
        title,
        icon: 'icon-icon-test',
      },
      component: viewConfig[name] ? viewConfig[name] : notFoundVue,
    };
    children && Array.isArray(children) && (item.children = menuItemChildren(children));
    return item;
  });

export const getCurUserRole = () => {
  if (window.location.pathname === '/login') {
    return [];
  }
  //取当前用户的role
  const role = JSON.parse(window.localStorage.getItem('userInfo'))?.role;

  if (!role) {
    router.replace('/login');
    return [];
  }
  //获取当前用户的权限
  const curUserRole = authList.find(val => val.role === role);

  //获取当前用户的视图权限
  const viewsRole = menuItemChildren(curUserRole?.viewList);
  return viewsRole;
};

export const addRoutes = (routes: any[]) => {
  routes.forEach(item => {
    router.addRoute('index', item);
  });
};
export const getBaseRoutes = () => {
  return baseRouter;
};

addRoutes(getCurUserRole());

// router.beforeEach((from, to, next) => {
//   console.log('111111');
//   next();
// })

export default router;
