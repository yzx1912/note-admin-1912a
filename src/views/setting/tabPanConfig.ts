export const tabPanData = [
  {
    tab: '系统设置',
    key: 'system',
    content: [
      {
        label: '系统地址',
        type: 'input',
        key: 'systemUrl',
      },
      {
        label: '后台地址',
        type: 'input',
        key: 'adminSystemUrl',
      },
      {
        label: '系统标题',
        type: 'input',
        key: 'systemTitle',
      },
      {
        label: 'Logo',
        type: 'input',
        key: 'systemLogo',
      },
      {
        label: 'Favicon',
        type: 'input',
        key: 'systemFavicon',
      },
      {
        label: '页脚信息',
        type: 'textarea',
        key: 'systemFooterInfo',
      },
    ],
    active: [
      {
        title: '保存',
        type: 'primary',
        click: val => {
          console.log(val);
        },
      },
    ],
  },
  {
    tab: '国际化设置',
    key: 'i18n',
  },
  {
    tab: 'SEO设置',
    key: 'seo',
    content: [
      {
        label: '关键词',
        type: 'input',
        key: 'seoKeyword',
      },
      {
        label: '描述信息',
        type: 'input',
        key: 'seoDesc',
      },
    ],
    active: [
      {
        title: '保存',
        type: 'primary',
        click: () => {
          console.log(11);
        },
      },
    ],
  },
  {
    tab: '数据统计',
    key: 'analy',
    content: [
      {
        label: '百度统计',
        type: 'input',
        key: 'baiduAnalyticsId',
      },
      {
        label: '谷歌分析',
        type: 'input',
        key: 'googleAnalyticsId',
      },
    ],
    active: [
      {
        title: '保存',
        type: 'primary',
        click: () => {
          console.log(11);
        },
      },
    ],
  },
  {
    tab: 'OSS设置',
    key: 'oss',
  },
  {
    tab: 'SMTP服务',
    key: 'smtp',
    content: [
      {
        label: 'SMTP地址',
        type: 'input',
        key: 'smtpHost',
      },
      {
        label: 'SMTP 端口 （强制使用SSL链接）',
        type: 'input',
        key: 'smtpPort',
      },
      {
        label: 'SMTP 用户',
        type: 'input',
        key: 'smtpUser',
      },
      {
        label: 'SMTP 密码',
        type: 'input',
        key: 'smtpPass',
      },
      {
        label: '发件人',
        type: 'input',
        key: 'smtpFromUser',
      },
    ],
    active: [
      {
        title: '保存',
        type: 'primary',
        click: () => {
          console.log(11);
        },
      },
      {
        title: '测试',
        click: () => {
          console.log(11);
        },
      },
    ],
  },
];
