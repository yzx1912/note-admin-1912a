import knowledge from './knowledge/index.vue';
import view from './view/index.vue';
import mail from './mail/index.vue';
import poster from './poster/index.vue';
import user from './user/index.vue';
import setting from './setting/index.vue';
import search from './search/index.vue';
import file from './file/index.vue';
import comment from './comment/index.vue';
import page from './page/index.vue';
import article from './article/index.vue';
import articleAll from './article/all/index.vue';
import articleTags from './article/tags/index.vue';
import articleCategory from './article/category/index.vue';
// 所有视图的出口

export default {
  knowledge,
  view,
  mail,
  poster,
  user,
  setting,
  search,
  file,
  comment,
  page,
  article,
  articleAll,
  articleTags,
  articleCategory,
};
