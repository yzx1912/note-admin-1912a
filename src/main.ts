import '@ant-design-vue/pro-layout/dist/style.css';
import 'ant-design-vue/dist/antd.variable.min.css';
import './style/common.less';
import { createApp } from 'vue';
import { ConfigProvider } from 'ant-design-vue';
import { createPinia } from 'pinia';
import ProLayout, { PageContainer } from '@ant-design-vue/pro-layout';
import { VueClipboard } from '@soerenmartius/vue3-clipboard';
import router from './router';

import App from './App.vue';

createApp(App)
  .use(createPinia())
  .use(router)
  .use(ConfigProvider)
  .use(ProLayout)
  .use(PageContainer)
  .use(VueClipboard)
  .mount('#app');
