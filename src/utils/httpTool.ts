import axios, { AxiosRequestConfig } from 'axios';
import { message } from 'ant-design-vue';
import CODEMAP from './codeMap';

export interface PageNationParams {
  page?: number;
  pageSize?: number;
}
export interface Params extends PageNationParams {
  [propertyName: string]: string | number | boolean | undefined;
}
const httpTool = axios.create({
  timeout: 10000,
  // baseURL: process.env.VUE_APP_BASEURL,
});
const wihleList = ['/api/auth/login'];

httpTool.interceptors.request.use(
  function (config: AxiosRequestConfig) {
    // 在发送请求之前做些什么
    if (wihleList.includes(config.url)) {
      return config;
    }
    config.headers &&
      (config.headers.Authorization = `Bearer ${JSON.parse(window.localStorage.getItem('userInfo'))?.token}`);
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  },
);

// 响应拦截
httpTool.interceptors.response.use(
  response => {
    if (response.data?.success) {
      return response.data;
    }
    message.error(response.data?.msg || CODEMAP[response.data?.statusCode || 500]);
    return response;
  },
  error => {
    if (error.code === 'ECONNABORTED') {
      // 网络请求超时
      message.error('您当前网络环境不好，请刷新重试~');
    } else {
      message.error(
        error.response?.data.msg || error.response?.statusText || CODEMAP[error.response.data?.status || 400],
      );
    }
    return Promise.reject(error);
  },
);

export default {
  ...httpTool,
  get(url: string, params: Params = {}) {
    return httpTool.get(url, {
      params,
    });
  },
  post(url: string, data = {}) {
    return httpTool.post(url, data);
  },
  patch(id: string, params: any) {
    return httpTool.patch(id, params);
  },
  delete(id: string) {
    return httpTool.delete(id);
  },
};
