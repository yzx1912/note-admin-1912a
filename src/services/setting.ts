import http from '@/utils/httpTool';

export const getSettingData = () => http.post('/api/setting/get');
