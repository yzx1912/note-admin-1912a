import http from '@/utils/httpTool';
// import { Params } from "@/utils/httpTool"
//登录
export const login = (data: any) => http.post('/api/auth/login', data);
//注册
export const register = (data: any) => http.post('/api/user/register', data);
// 页面管理
export const getPage = (params: any) => http.get('/api/page', params);
//页面管理删除
export const delPage = (id: string) => http.delete(`/api/page/${id}`);

///api/article  工作台 最新文章
export const NewArticle = (params: any) => http.get('api/article', params);
//api/comment  工作台 最新评论
export const NewComment = (params: any) => http.get('/api/comment ', params);
//最新评论 点击删除页面
export const NewCommentDel = (id: string) => http.delete(`/api/comment/${id}`)

// 用户管理
export const getUserList = (params: any) => http.get('/api/user', params);
//用户管理的 启用禁用
export const getUserDisabled = (params: any) => http.post(`/api/user/update`, params);
//用户管理发布功能
export const pageStatus = (id: string, params: any) => http.patch(`/api/page/${id}`, params)

//评论管理
export const getComment = (params = { page: 1, pageSize: 12 }) => http.get('/api/comment', params);
//评论通过功能
export const AddPass = (id: string, params: any) => http.patch(`/api/comment/${id}`, params);
//评论拒绝
export const RefusePass = (id: string, params: any) => http.patch(`/api/comment/${id}`, params);
//评论页面的搜索
export const searchComment = (params: { name?: any; email?: string; pass?: number; page: number; pageSize: number }) =>
  http.get('api/comment', params);
//评论删除
export const delComment = (id: string) => http.delete(`/api/comment/${id}`);
//评论回复，新增
export const AddComment = (params: any) => http.post('/api/comment', params);

// 搜索记录
export const getSearch = (params: any) => http.get('/api/search', params);
//搜索页面的删除
export const getSearchDel = (id: string) => http.delete(`/api/search/${id}`)

// 访问统计
export const getView = (params: any) => http.get('/api/view', params);
//访问统计的删除
export const getViewDel = (id: string) => http.delete(`/api/view/${id}`)

// 知识小册列表数据
export const getskList = (params: any) => http.get('/api/knowledge', params);
// 发布线上/设为草稿
export const getStatus = (id: string, params: any) => http.patch(`/api/knowledge/${id}`, params);
// 小册设置
export const updatedKnowledge = (id: string, params: any) => http.patch(`/api/knowledge/${id}`, params);
// 知识小册删除
export const delKnowledge = (id: string) => http.delete(`/api/knowledge/${id}`)
// 小册新建
export const addKnowledge = (params:any) => http.post('/api/knowledge/book',params)
// 小册编辑跳详情
export const editKnowledge = (id: string) => http.get(`/api/knowledge/${id}`)

///api/tag 标签管理
export const TagLabel = (params: any) => http.get('/api/tag', params);
///api/tag 标签管理保存
export const addTagLabel = (params: any) => http.post('/api/tag', params);
//标签管理 删除
export const delTagLabel = (id:string) => http.delete(`/api/tag/${id}`)
//标签管理 更新
export const getTagSave = (id: string,params:any) => http.patch(`/api/tag/${id}`,params)
//api/category 分类管理
export const CategoryLabel = (params: any) => http.get('/api/category', params);
//api/category 分类管理保存
export const addLabel = (params: any) => http.post('/api/category', params);
//标签管理 删除
export const delCategoryLabel = (id: string) => http.delete(`/api/category/${id}`);
//分类管理更新
// export const getSave = (id:string,params: any) => http.patch('/api/category'+id, params)

export const getCategorySave = (id:string,params: any) => http.patch(`/api/category/${id}`, params)

//所有文章
export const getArticleAll = (params: any) => http.get('/api/article', params);
//所有文章首焦功能
export const articleRecommend = (id: string, params: any) => http.patch(`/api/article/${id}`, params)
// 所有文章的删除
export const articleDel = (id: string) => http.delete(`/api/article/${id}`)
// 发布线上/设为草稿
export const getArticleStatus = (id: string, params: any) => http.patch(`/api/article/${id}`, params)

//获取文件
export const getFile = (params: any) => http.get('/api/file', params)
// 文件页面的删除 DELETE  /api/file/33b0d7b7-5ff1-4f2e-8b63-378886dde30b
export const getFileDel = (id: string) => http.delete(`/api/file/${id}`);

//邮件页面 /api/smtp
export const getSmtp = (params: any) => http.get('/api/smtp', params);
//海报页面 /api/poster
export const getPoster = (params: any) => http.get('/api/poster', params)

//个人资料基本设置
export const setBasic = (params: any) => http.post('/api/user/update', params);
